from django.shortcuts import render
from rest_framework import viewsets
from .models import BookCollection
from .serializers import BookCollectionSerializer


class BookViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    serializer_class = BookCollectionSerializer
    queryset = BookCollection.objects.all()
