from rest_framework import serializers
from .models import BookCollection


class BookCollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookCollection
        fields = [
            'id',
            'name',
            'author',
            'publisher',
            'isbn',
            'publish_year',
            'category',
            'price',
            'description',
            'file',
            'created_at',
            'updated_at'
        ]
        read_only_fields = ['id', 'created_at', 'updated_at']

